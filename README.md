# TP4 : CI/CD

# Sommaire

- [TP4 : CI/CD](#tp4--cicd)
- [Sommaire](#sommaire)
- [2. Où sont exécutées les commandes](#2-où-sont-exécutées-les-commandes)
- [3. Préparer une VM](#3-préparer-une-vm)
- [4. Appliqué à Ansible](#4-appliqué-à-ansible)
  - [A. Tests](#a-tests)
  - [B. Deploy](#b-deploy)
- [5. Appliqué à Docker](#5-appliqué-à-docker)

# 2. Où sont exécutées les commandes

- **Log déploiement avec image `alpine`**

```
Running with gitlab-runner 15.9.0~beta.115.g598a7c91 (598a7c91)
  on blue-3.shared.runners-manager.gitlab.com/default zxwgkjAP, system ID: s_284de3abf026
  feature flags: FF_USE_IMPROVED_URL_MASKING:true
Preparing the "docker+machine" executor
00:07
Using Docker executor with image alpine ...
Pulling docker image alpine ...
Using docker image sha256:9ed4aefc74f6792b5a804d1d146fe4b4a2299147b0f50eaf2b08435d7b38c27e for alpine with digest alpine@sha256:124c7d2707904eea7431fffe91522a01e5a861a624ee31d03372cc1d138a3126 ...
Preparing environment
00:01
Running on runner-zxwgkjap-project-43904613-concurrent-0 via runner-zxwgkjap-shared-1681201406-92c54cbf...
Getting source from Git repository
00:01
$ eval "$CI_PRE_CLONE_SCRIPT"
Fetching changes with git depth set to 20...
Initialized empty Git repository in /builds/EPEYRATAUD/b3-cloud/.git/
Created fresh repository.
Checking out a30fa68e as detached HEAD (ref is main)...
Skipping Git submodules setup
Executing "step_script" stage of the job script
00:01
Using docker image sha256:9ed4aefc74f6792b5a804d1d146fe4b4a2299147b0f50eaf2b08435d7b38c27e for alpine with digest alpine@sha256:124c7d2707904eea7431fffe91522a01e5a861a624ee31d03372cc1d138a3126 ...
$ cat /etc/os-release
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.17.3
PRETTY_NAME="Alpine Linux v3.17"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://gitlab.alpinelinux.org/alpine/aports/-/issues"
Cleaning up project directory and file based variables
00:01
Job succeeded
```

# 3. Préparer une VM

📁 Dossier **`terraform`**

https://gitlab.com/EPEYRATAUD/b3-cloud/-/tree/main/terraform

Il y a un gitignore dans lequel j'ai mis les fichiers "useless" du dossier **terraform**, donc normal qu'il n'y ait pas tout :) .

# 4. Appliqué à Ansible

## A. Tests

📃 Fichier **`.gitlab-ci.yml`**

https://gitlab.com/EPEYRATAUD/b3-cloud/-/blob/main/.gitlab-ci.yml

## B. Deploy

- _Récupération du provider azurerm :_

```
PS C:\Users\lucas\B3-2022-2023\gitlab\b3-cloud\terraform> terraform init

Initializing the backend...

Initializing provider plugins...
- Finding hashicorp/azurerm versions matching ">= 3.0.0"...
- Installing hashicorp/azurerm v3.51.0...
- Installed hashicorp/azurerm v3.51.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

[...]
```

- _Vérification de la validité du plan :_

```
PS C:\Users\lucas\B3-2022-2023\gitlab\b3-cloud\terraform> terraform plan
azurerm_resource_group.rg-b3-vm1: Refreshing state... [id=/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1]

Note: Objects have changed outside of Terraform

Terraform detected the following changes made outside of Terraform since the last "terraform apply" which may have affected this plan:

  # azurerm_resource_group.rg-b3-vm1 has been deleted
  - resource "azurerm_resource_group" "rg-b3-vm1" {
        id       = "/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1"
      - location = "eastus" -> null
      - name     = "b3-vm1" -> null
        tags     = {}
    }


Unless you have made equivalent changes to your configuration, or ignored the relevant attributes using ignore_changes, the following plan may include actions to undo or respond to these changes.

────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # azurerm_linux_virtual_machine.vm-b3-vm1 will be created
  + resource "azurerm_linux_virtual_machine" "vm-b3-vm1" {
      + admin_username                  = "targa"
      + allow_extension_operations      = true
      + computer_name                   = (known after apply)
      + disable_password_authentication = true
      + extensions_time_budget          = "PT1H30M"
      + id                              = (known after apply)
      + location                        = "eastus"
      + max_bid_price                   = -1
      + name                            = "b3-vm1"
      + network_interface_ids           = (known after apply)
      + patch_assessment_mode           = "ImageDefault"
      + patch_mode                      = "ImageDefault"
      + platform_fault_domain           = -1
      + priority                        = "Regular"
      + private_ip_address              = (known after apply)
      + private_ip_addresses            = (known after apply)
      + provision_vm_agent              = true
      + public_ip_address               = (known after apply)
      + public_ip_addresses             = (known after apply)
      + resource_group_name             = "b3-vm1"
      + size                            = "Standard_B1s"
      + virtual_machine_id              = (known after apply)

[...]

  # azurerm_virtual_network.vn-b3-vm1 will be created
  + resource "azurerm_virtual_network" "vn-b3-vm1" {
      + address_space       = [
          + "10.0.0.0/16",
        ]
      + dns_servers         = (known after apply)
      + guid                = (known after apply)
      + id                  = (known after apply)
      + location            = "eastus"
      + name                = "b3-vm1"
      + resource_group_name = "b3-vm1"
      + subnet              = (known after apply)
    }

Plan: 6 to add, 0 to change, 0 to destroy.
```

- _Déploiement du plan :_

```
PS C:\Users\lucas\B3-2022-2023\gitlab\b3-cloud\terraform> terraform apply
azurerm_resource_group.rg-b3-vm1: Refreshing state... [id=/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1]

Note: Objects have changed outside of Terraform

Terraform detected the following changes made outside of Terraform since the last "terraform apply" which may have affected this plan:

  # azurerm_resource_group.rg-b3-vm1 has been deleted
  - resource "azurerm_resource_group" "rg-b3-vm1" {
        id       = "/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1"
      - location = "eastus" -> null
      - name     = "b3-vm1" -> null
        tags     = {}
    }


Unless you have made equivalent changes to your configuration, or ignored the relevant attributes using ignore_changes, the following plan may include actions to undo or respond to these changes.

────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # azurerm_linux_virtual_machine.vm-b3-vm1 will be created
  + resource "azurerm_linux_virtual_machine" "vm-b3-vm1" {
      + admin_username                  = "targa"
      + allow_extension_operations      = true
      + computer_name                   = (known after apply)
      + disable_password_authentication = true
      + extensions_time_budget          = "PT1H30M"
      + id                              = (known after apply)
      + location                        = "eastus"
      + max_bid_price                   = -1
      + name                            = "b3-vm1"
      + network_interface_ids           = (known after apply)
      + patch_assessment_mode           = "ImageDefault"
      + patch_mode                      = "ImageDefault"
      + platform_fault_domain           = -1
      + priority                        = "Regular"
      + private_ip_address              = (known after apply)
      + private_ip_addresses            = (known after apply)
      + provision_vm_agent              = true
      + public_ip_address               = (known after apply)
      + public_ip_addresses             = (known after apply)
      + resource_group_name             = "b3-vm1"
      + size                            = "Standard_B1s"
      + virtual_machine_id              = (known after apply)

[...]

  # azurerm_virtual_network.vn-b3-vm1 will be created
  + resource "azurerm_virtual_network" "vn-b3-vm1" {
      + address_space       = [
          + "10.0.0.0/16",
        ]
      + dns_servers         = (known after apply)
      + guid                = (known after apply)
      + id                  = (known after apply)
      + location            = "eastus"
      + name                = "b3-vm1"
      + resource_group_name = "b3-vm1"
      + subnet              = (known after apply)
    }
Do you want to perform these actions?
  Terraform will perform the actions described above.

  Enter a value: yes

azurerm_resource_group.rg-b3-vm1: Creating...
azurerm_resource_group.rg-b3-vm1: Creation complete after 2s [id=/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1]
azurerm_public_ip.b3-vm1-public-ip: Creation complete after 5s [id=/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1/providers/Microsoft.Network/publicIPAddresses/b3-vm1-public-ip]
azurerm_virtual_network.vn-b3-vm1: Creation complete after 8s [id=/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1/providers/Microsoft.Network/virtualNetworks/b3-vm1]
azurerm_subnet.s-b3-vm1: Creating...
azurerm_subnet.s-b3-vm1: Creation complete after 7s [id=/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1/providers/Microsoft.Network/virtualNetworks/b3-vm1/subnets/internal]
azurerm_network_interface.nic-b3-vm1: Creating...
azurerm_network_interface.nic-b3-vm1: Creation complete after 4s [id=/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1/providers/Microsoft.Network/networkInterfaces/nic-b3-vm1]
azurerm_linux_virtual_machine.vm-b3-vm1: Creating...
azurerm_linux_virtual_machine.vm-b3-vm1: Still creating... [10s elapsed]
azurerm_linux_virtual_machine.vm-b3-vm1: Still creating... [20s elapsed]
azurerm_linux_virtual_machine.vm-b3-vm1: Still creating... [30s elapsed]
azurerm_linux_virtual_machine.vm-b3-vm1: Still creating... [40s elapsed]
azurerm_linux_virtual_machine.vm-b3-vm1: Still creating... [50s elapsed]
azurerm_linux_virtual_machine.vm-b3-vm1: Creation complete after 52s [id=/subscriptions/6a8c9233-ddbd-4769-b389-a6cbabe5acac/resourceGroups/b3-vm1/providers/Microsoft.Compute/virtualMachines/b3-vm1]

Apply complete! Resources: 6 added, 0 changed, 0 destroyed.d.
```

- Log du lancement playbook au stage `syntax_check`

```
Running with gitlab-runner 15.9.0~beta.115.g598a7c91 (598a7c91)
  on blue-2.shared.runners-manager.gitlab.com/default XxUrkriX, system ID: s_90897a2659b5
  feature flags: FF_USE_IMPROVED_URL_MASKING:true
Preparing the "docker+machine" executor
00:25
Using Docker executor with image cytopia/ansible:latest ...
Pulling docker image cytopia/ansible:latest ...
Using docker image sha256:5b0ac1cac0da48bb4887791f8cdff4d580d714fd5ed89f60caa02bd146994d75 for cytopia/ansible:latest with digest cytopia/ansible@sha256:c503a8e7baf97913dc2e8151161d42ea75951b504c37380215d5d9858f9478c5 ...
Preparing environment
00:03
Running on runner-xxurkrix-project-43904613-concurrent-0 via runner-xxurkrix-shared-1681245239-5350fc5c...
Getting source from Git repository
00:02
$ eval "$CI_PRE_CLONE_SCRIPT"
Fetching changes with git depth set to 20...
Initialized empty Git repository in /builds/EPEYRATAUD/b3-cloud/.git/
Created fresh repository.
Checking out efbdff12 as detached HEAD (ref is main)...
Skipping Git submodules setup
Executing "step_script" stage of the job script
00:01
Using docker image sha256:5b0ac1cac0da48bb4887791f8cdff4d580d714fd5ed89f60caa02bd146994d75 for cytopia/ansible:latest with digest cytopia/ansible@sha256:c503a8e7baf97913dc2e8151161d42ea75951b504c37380215d5d9858f9478c5 ...
$ cd ansible/
$ ansible-playbook playbooks/first.yml --syntax-check
[WARNING]: Ansible is being run in a world writable directory
(/builds/EPEYRATAUD/b3-cloud/ansible), ignoring it as an ansible.cfg source.
For more information see
https://docs.ansible.com/ansible/devel/reference_appendices/config.html#cfg-in-
world-writable-dir
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that
the implicit localhost does not match 'all'
[WARNING]: Could not match supplied host pattern, ignoring: azure
playbook: playbooks/first.yml
Cleaning up project directory and file based variables
00:01
Job succeeded
```

- Connexion `SSH`

```
PS C:\Users\lucas> ssh -i C:\Users\lucas\.ssh\id_rsa_TP4 targa@13.92.197.64
Welcome to Ubuntu 18.04.6 LTS (GNU/Linux 5.4.0-1105-azure x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Wed Apr 12 15:54:38 UTC 2023

  System load:  0.08              Processes:           105
  Usage of /:   5.4% of 28.89GB   Users logged in:     0
  Memory usage: 23%               IP address for eth0: 10.0.2.4
  Swap usage:   0%

[...]


Last login: Wed Apr 12 15:42:33 2023 from 176.175.97.4
```

- **Gérer le `become: true`**

```
targa@b3-vm1:~$ sudo cat /etc/sudoers
#
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults        env_reset
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL

# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) NOPASSWD: ALL

# See sudoers(5) for more information on "#include" directives:

#includedir /etc/sudoers.d
```

- test après modification du fichier `sudoers` pour sudo sans password

```
targa@b3-vm1:~$ sudo ls -al /root
total 24
drwx------  4 root root 4096 Apr 12 17:14 .
drwxr-xr-x 23 root root 4096 Apr 12 16:31 ..
-rw-r--r--  1 root root 3106 Apr  9  2018 .bashrc
drwxr-xr-x  3 root root 4096 Apr 12 17:14 .local
-rw-r--r--  1 root root  148 Aug 17  2015 .profile
drwx------  2 root root 4096 Apr 12 16:31 .ssh
```

- Log du lancement playbook au stage `deploy`

```
Running with gitlab-runner 15.9.0~beta.115.g598a7c91 (598a7c91)
  on blue-1.shared.runners-manager.gitlab.com/default j1aLDqxS, system ID: s_b437a71a38f9
  feature flags: FF_USE_IMPROVED_URL_MASKING:true
Preparing the "docker+machine" executor
Using Docker executor with image cytopia/ansible:latest ...
Pulling docker image cytopia/ansible:latest ...
Using docker image sha256:5b0ac1cac0da48bb4887791f8cdff4d580d714fd5ed89f60caa02bd146994d75 for cytopia/ansible:latest with digest cytopia/ansible@sha256:c503a8e7baf97913dc2e8151161d42ea75951b504c37380215d5d9858f9478c5 ...
Preparing environment
00:03
Running on runner-j1aldqxs-project-43904613-concurrent-0 via runner-j1aldqxs-shared-1681247262-0cb94aab...
Getting source from Git repository
00:01
$ eval "$CI_PRE_CLONE_SCRIPT"
Fetching changes with git depth set to 20...
Initialized empty Git repository in /builds/EPEYRATAUD/b3-cloud/.git/
Created fresh repository.
Checking out 6917e87a as detached HEAD (ref is main)...
Skipping Git submodules setup
Executing "step_script" stage of the job script
00:02
Using docker image sha256:5b0ac1cac0da48bb4887791f8cdff4d580d714fd5ed89f60caa02bd146994d75 for cytopia/ansible:latest with digest cytopia/ansible@sha256:c503a8e7baf97913dc2e8151161d42ea75951b504c37380215d5d9858f9478c5 ...
$ mkdir ~/.ssh
$ echo $SSH_PRIVATE_KEY > ~/.ssh/id_rsa
$ ansible-playbook ansible/playbooks/first.yml
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that
the implicit localhost does not match 'all'
[WARNING]: Could not match supplied host pattern, ignoring: azure
PLAY [Install vim] *************************************************************
skipping: no hosts matched
PLAY RECAP *********************************************************************
Cleaning up project directory and file based variables
00:01
Job succeeded
```
